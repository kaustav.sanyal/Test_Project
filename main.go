package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var (
	db *sql.DB
)

type todo struct {
	Name     string
	Due      string
	Complete bool
}

func main() {
	var err error
	db, err = sql.Open("postgres", os.Getenv("https://infinite-depths-35560.herokuapp.com"))
	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}

	router := gin.New()
	router.Use(gin.Logger())
	router.LoadHTMLGlob("templates/*.tmpl.html")
	router.Static("/static", "static")

	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl.html", nil)
	})

	router.GET("/repeat", repeatFunc)
	router.GET("/db", dbFunc)
	router.POST("/todoitem", addTodoItem)
	router.GET("/todoitems", getodoitems)

	router.Run(":" + port)
}
